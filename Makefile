
NAME := maths-the-fun-parts


all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		--exclude .node_modules \
		--exclude code \
		--exclude code-ts \
		./ \
		dreamhost:artificialworlds.net/presentations/${NAME}/
