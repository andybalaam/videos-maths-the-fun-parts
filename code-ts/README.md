# Code for Maths: the fun parts

## Prep

```
npm install
```

## Test

```
npm run test
```

## Build and Run

```
npm run dev
```

## Project setup

Here is how I set up this project:

```
nvm install 18
npm create vite@latest maths-fun -- --template react-swc-ts
npm install -D vitest
npm init @eslint/config
npm install prettier eslint-config-prettier --save-dev
```

Then I added the "test" script to package.json, which just runs `vitest run`
and updated the "build" script to run eslint and prettier, and made a
.prettierrc file.
