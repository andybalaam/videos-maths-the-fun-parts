import { describe, expect, test } from "vitest";

import {
    PlusMod5,
    is_closed,
    has_correct_identity,
    IterableGroup,
    has_all_inverses,
    is_associative,
    Group,
    is_closed_it,
    has_correct_identity_it,
    has_all_inverses_it,
    is_associative_it,
    D6,
    D6Members,
} from "./group.js";
import { ArrSet, EmptySet, IterableSet, Set } from "./set.js";

/**
 * Not a group because not closed
 */
class PlusNoMod extends PlusMod5 {
    op(left: any, right: any) {
        return left + right;
    }
}

/**
 * Not a group because identity is wrong
 */
class PlusBadIdentity extends PlusMod5 {
    identity() {
        return 3;
    }
}

class TimesMod5 extends IterableGroup {
    iterable_set(): IterableSet {
        return new ArrSet([0, 1, 2, 3, 4]);
    }

    op(left: any, right: any) {
        return (left * right) % 5;
    }

    identity() {
        return 1;
    }
}

class MinusMod5 extends IterableGroup {
    iterable_set(): IterableSet {
        return new ArrSet([0, 1, 2, 3, 4]);
    }

    op(left: any, right: any) {
        return (left - right) % 5;
    }

    identity() {
        return 0;
    }
}

const letters_re = /^[a-zA-Z]*$/;

class LetterStrings implements Set {
    contains(element: any): boolean {
        return (
            typeof element === "string" && element.match(letters_re) !== null
        );
    }
}

class Cat implements Group {
    set(): Set {
        return new LetterStrings();
    }
    op(left: any, right: any) {
        return left + right;
    }
    identity() {
        return "";
    }
}

test("PlusMod5 operation works", () => {
    const g = new PlusMod5();
    expect(g.op(1, 2)).toBe(3);
    expect(g.op(4, 3)).toBe(2);
});

test("PlusMod5 is closed", () => {
    const g = new PlusMod5();
    expect(is_closed(g)).toBe(true);
});

test("PlusNoMod is not closed", () => {
    const g = new PlusNoMod();
    expect(is_closed(g)).toBe(false);
});

test("PlusMod5's identity leaves elements unchanged", () => {
    const g = new PlusMod5();
    expect(has_correct_identity(g)).toBe(true);
});

test("PlusBadIdentity has a bad identity", () => {
    const g = new PlusBadIdentity();
    expect(has_correct_identity(g)).toBe(false);
});

test("PlusMod5 has all inverses", () => {
    const g = new PlusMod5();
    expect(has_all_inverses(g)).toBe(true);
});

test("TimesMod5 does not have inverses", () => {
    const g = new TimesMod5();
    expect(has_all_inverses(g)).toBe(false);
});

test("PlusMod5 is associative", () => {
    const g = new PlusMod5();
    expect(is_associative(g)).toBe(true);
});

test("MinusMod5 is not associative", () => {
    const g = new MinusMod5();
    expect(is_associative(g)).toBe(false);
});

describe("Cat", () => {
    const letter_string_examples = ["", "a", "A", "aA", "gFd"];

    test("Only strings are members of LetterStrings", () => {
        const s = new LetterStrings();

        expect(s.contains(4)).toBe(false);
        expect(s.contains(new EmptySet())).toBe(false);
        expect(s.contains(" ")).toBe(false);

        for (const member of letter_string_examples) {
            expect(s.contains(member)).toBe(true);
        }
    });

    test("Cat is closed", () => {
        const g = new Cat();
        expect(is_closed_it(g, letter_string_examples)).toBe(true);
    });

    test("Cat has identity", () => {
        const g = new Cat();
        expect(has_correct_identity_it(g, letter_string_examples)).toBe(true);
    });

    test("Cat does not have inverses", () => {
        const g = new Cat();
        expect(has_all_inverses_it(g, letter_string_examples)).toBe(false);
    });

    test("Cat is_associative", () => {
        const g = new Cat();
        expect(is_associative_it(g, letter_string_examples)).toBe(true);
    });
});

describe("D6", () => {
    test("Transforms ABC as expected", () => {
        const s = new D6();
        expect(s.op("ABC", D6Members.identity)).toEqual("ABC");
        expect(s.op("ABC", D6Members.flip_vertical)).toEqual("ACB");
        expect(s.op("ABC", D6Members.flip_tlbr)).toEqual("BAC");
        expect(s.op("ABC", D6Members.flip_trbl)).toEqual("CBA");
        expect(s.op("ABC", D6Members.rot120)).toEqual("CAB");
        expect(s.op("ABC", D6Members.rot240)).toEqual("BCA");
    });

    test("Two rot120s == rot240", () => {
        const s = new D6();
        expect(s.op(s.op("ABC", D6Members.rot120), D6Members.rot120)).toEqual(
            D6Members.rot240
        );

        expect(s.op(s.op("ABC", D6Members.rot240), D6Members.rot120)).toEqual(
            D6Members.identity
        );
    });

    test("Two flips == identity", () => {
        const s = new D6();
        expect(
            s.op(s.op("ABC", D6Members.flip_vertical), D6Members.flip_vertical)
        ).toEqual(D6Members.identity);

        expect(
            s.op(s.op("ABC", D6Members.flip_tlbr), D6Members.flip_tlbr)
        ).toEqual(D6Members.identity);

        expect(
            s.op(s.op("ABC", D6Members.flip_trbl), D6Members.flip_trbl)
        ).toEqual(D6Members.identity);
    });

    test("D6 is closed", () => {
        const g = new D6();
        expect(is_closed(g)).toBe(true);
    });

    test("D6 has identity", () => {
        const g = new D6();
        expect(has_correct_identity(g)).toBe(true);
    });

    test("D6 has all inverses", () => {
        const g = new D6();
        expect(has_all_inverses(g)).toBe(true);
    });

    test("D6 is associative", () => {
        const g = new D6();
        expect(is_associative(g)).toBe(true);
    });

    test("D6 is not commutative", () => {
        const g = new D6();
        expect(g.op(D6Members.flip_trbl, D6Members.rot120)).not.toEqual(
            g.op(D6Members.rot120, D6Members.flip_trbl)
        );
    });
});
