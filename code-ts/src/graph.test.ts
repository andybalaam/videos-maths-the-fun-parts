import { describe, expect, test } from "vitest";

import {
    K3,
    is_connected,
    NoEdges3,
    all_paths_from,
    has_eulerian_circuit,
    FiniteGraph,
    reachable_from,
    has_eulerian_circuit_slow,
    random_graph,
} from "./graph.js";

class Star implements FiniteGraph<number> {
    vertices(): number[] {
        return [1, 2, 3, 4, 100];
    }
    edges(): [number, number][] {
        return [
            [1, 100], // 1 ------> 100
            [2, 100], // 2 <------ 100
            [2, 3], // 2 -> 3
            [3, 100], //      3 -> 100
            [4, 100], //      4 <- 100
            [4, 1], // 1 <- 4
        ];
    }
}

class NoVertices implements FiniteGraph<number> {
    vertices(): number[] {
        return [1];
    }
    edges(): [number, number][] {
        return [];
    }
}

class OneVertexNoEdges implements FiniteGraph<number> {
    vertices(): number[] {
        return [1];
    }
    edges(): [number, number][] {
        return [];
    }
}

test("K3 is connected", () => {
    const g = new K3();
    expect(is_connected(g)).toBe(true);
});

test("Graph with no edges is disconnected", () => {
    const g = new NoEdges3();
    expect(is_connected(g)).toBe(false);
});

test("Graph with no vertices does not crash is_connected", () => {
    const g = new NoVertices();
    is_connected(g);
});

test("Graph with 1 vertex is connected", () => {
    const g = new OneVertexNoEdges();
    expect(is_connected(g)).toBe(true);
});

test("Graph with 2 separate circuits is disconnected", () => {
    class TwoCircuits implements FiniteGraph<number> {
        vertices(): number[] {
            return [1, 2, 3, 101, 102, 103];
        }
        edges(): [number, number][] {
            return [
                [1, 2],
                [1, 3],
                [2, 3],
                [101, 2],
                [101, 3],
                [102, 3],
            ];
        }
    }

    const g = new TwoCircuits();
    expect(is_connected(g)).toBe(false);
});

test("Can find all paths in K3", () => {
    const g = new K3();
    expect(all_paths_from(g, "a")).toStrictEqual([
        ["a"],
        ["a", "b"],
        ["a", "b", "c"],
        ["a", "b", "c", "a"],
        ["a", "c"],
        ["a", "c", "b"],
        ["a", "c", "b", "a"],
    ]);
});

test("Can find all paths in graph with no edges", () => {
    const g = new NoEdges3();
    expect(all_paths_from(g, 1)).toStrictEqual([[1]]);
});

test("K3 has an eulerian circuit", () => {
    const g = new K3();
    expect(has_eulerian_circuit(g)).toBe(true);
    expect(has_eulerian_circuit_slow(g)).toBe(true);
});

test("Disconnected graph has no eulerian circuit", () => {
    const g = new NoEdges3();
    expect(has_eulerian_circuit(g)).toBe(false);
    expect(has_eulerian_circuit_slow(g)).toBe(false);
});

test("Acyclic graph has no eulerian_circuit", () => {
    class Acyclic implements FiniteGraph<number> {
        vertices(): number[] {
            return [1, 2, 3];
        }
        edges(): [number, number][] {
            return [
                [1, 2],
                [2, 3],
            ];
        }
    }

    const g = new Acyclic();
    expect(has_eulerian_circuit(g)).toBe(false);
    expect(has_eulerian_circuit_slow(g)).toBe(false);
});

test("Single cycle graph has eulerian circuit", () => {
    class Acyclic implements FiniteGraph<number> {
        vertices(): number[] {
            return [1, 2, 3];
        }
        edges(): [number, number][] {
            return [
                [3, 2],
                [2, 1],
                [1, 3],
            ];
        }
    }

    const g = new Acyclic();
    expect(has_eulerian_circuit(g)).toBe(true);
    expect(has_eulerian_circuit_slow(g)).toBe(true);
});

test("Star is connected", () => {
    const g = new Star();
    expect(is_connected(g)).toBe(true);
});

test("reachable_from in star", () => {
    const g = new Star();
    expect(reachable_from(g, 1)).toStrictEqual([100, 4]);
    expect(reachable_from(g, 2)).toStrictEqual([100, 3]);
    expect(reachable_from(g, 3)).toStrictEqual([2, 100]);
    expect(reachable_from(g, 4)).toStrictEqual([100, 1]);
});

test("All paths through a star graph", () => {
    const g = new Star();
    expect(all_paths_from(g, 1)).toStrictEqual([
        [1],
        [1, 100],
        [1, 100, 2],
        [1, 100, 2, 3],
        [1, 100, 2, 3, 100],
        [1, 100, 2, 3, 100, 4],
        [1, 100, 2, 3, 100, 4, 1],
        [1, 100, 3],
        [1, 100, 3, 2],
        [1, 100, 3, 2, 100],
        [1, 100, 3, 2, 100, 4],
        [1, 100, 3, 2, 100, 4, 1],
        [1, 100, 4],
        [1, 100, 4, 1],
        [1, 4],
        [1, 4, 100],
        [1, 4, 100, 1],
        [1, 4, 100, 2],
        [1, 4, 100, 2, 3],
        [1, 4, 100, 2, 3, 100],
        [1, 4, 100, 2, 3, 100, 1],
        [1, 4, 100, 3],
        [1, 4, 100, 3, 2],
        [1, 4, 100, 3, 2, 100],
        [1, 4, 100, 3, 2, 100, 1],
    ]);
});

test("Star graph has eulerian circuit", () => {
    const g = new Star();
    expect(has_eulerian_circuit(g)).toBe(true);
    expect(has_eulerian_circuit_slow(g)).toBe(true);
});

test("has_eulerian_circuit agrees with the slow version", () => {
    let num_eul = 0;

    // Try lots of random graphs
    for (let i = 0; i < 2000; i++) {
        const graph = random_graph();
        // The two methods of calculating agree with each other
        const eul1 = has_eulerian_circuit(graph);
        const eul2 = has_eulerian_circuit_slow(graph);
        if (eul1 !== eul2) {
            console.log(`graph ${graph.edges()} ${eul1} ${eul2}`);
        }
        expect(eul1).toEqual(eul2);
        if (eul1) {
            num_eul++;
        }
    }

    // At least some of the random graphs are Eulerian
    expect(num_eul).toBeGreaterThan(20);
});

