import { ArrSet, IterableSet, Set } from "./set.js";

export interface Group {
    set(): Set;
    op(left: any, right: any): any;
    identity(): any;
}

export abstract class IterableGroup implements Group, Iterable<any> {
    set(): Set {
        return this.iterable_set();
    }

    [Symbol.iterator](): Iterator<any, any, undefined> {
        return this.iterable_set()[Symbol.iterator]();
    }

    abstract iterable_set(): IterableSet;
    abstract op(left: any, right: any): any;
    abstract identity(): any;
}

export class PlusMod5 extends IterableGroup {
    iterable_set(): IterableSet {
        return new ArrSet([0, 1, 2, 3, 4]);
    }

    op(left: any, right: any) {
        return (left + right) % 5;
    }

    identity() {
        return 0;
    }
}

export const D6Members = {
    identity: "ABC",
    flip_vertical: "ACB",
    flip_tlbr: "BAC",
    flip_trbl: "CBA",
    rot120: "CAB",
    rot240: "BCA",
};

export class D6 extends IterableGroup {
    iterable_set(): IterableSet {
        return new ArrSet([
            D6Members.identity,
            D6Members.flip_vertical,
            D6Members.flip_tlbr,
            D6Members.flip_trbl,
            D6Members.rot120,
            D6Members.rot240,
        ]);
    }

    op(left: any, right: any) {
        function tch(initial: string, act: string): string {
            switch (act) {
                case "A":
                    return initial[0];
                case "B":
                    return initial[1];
                case "C":
                    return initial[2];
                default:
                    throw new Error(`Character ${act} not allowed!`);
            }
        }

        function transform(initial: string, action: string): string {
            const ch1 = tch(initial, action[0]);
            const ch2 = tch(initial, action[1]);
            const ch3 = tch(initial, action[2]);
            return `${ch1}${ch2}${ch3}`;
        }

        return transform(left, right);
    }

    identity() {
        return D6Members.identity;
    }
}

export function is_closed(group: IterableGroup): boolean {
    return is_closed_it(group, group);
}

export function is_closed_it(group: Group, members: Iterable<any>): boolean {
    for (const x of members) {
        for (const y of members) {
            const z = group.op(x, y);
            if (!group.set().contains(z)) {
                return false;
            }
        }
    }
    return true;
}

export function has_correct_identity(group: IterableGroup): boolean {
    return has_correct_identity_it(group, group);
}

export function has_correct_identity_it(
    group: Group,
    members: Iterable<any>
): boolean {
    const idt = group.identity();
    for (const x of members) {
        if (group.op(idt, x) !== x) return false;
        if (group.op(x, idt) !== x) return false;
    }
    return true;
}

export function has_all_inverses(group: IterableGroup): boolean {
    return has_all_inverses_it(group, group);
}

export function has_all_inverses_it(
    group: Group,
    members: Iterable<any>
): boolean {
    for (const x of members) {
        if (!has_inverse_it(x, group, members)) {
            return false;
        }
    }
    return true;
}

function has_inverse_it(
    element: any,
    group: Group,
    members: Iterable<any>
): boolean {
    const idt = group.identity();
    for (const y of members) {
        if (group.op(element, y) === idt && group.op(y, element) === idt) {
            return true;
        }
    }
    return false;
}

export function is_associative(group: IterableGroup): boolean {
    return is_associative_it(group, group);
}

export function is_associative_it(
    group: Group,
    members: Iterable<any>
): boolean {
    for (const x of members) {
        for (const y of members) {
            for (const z of members) {
                if (
                    group.op(x, group.op(y, z)) !== group.op(group.op(x, y), z)
                ) {
                    return false;
                }
            }
        }
    }
    return true;
}
