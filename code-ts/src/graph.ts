export interface FiniteGraph<V> {
    vertices(): Array<V>;
    edges(): Array<[V, V]>;
}

export class K3 implements FiniteGraph<string> {
    vertices(): Array<string> {
        return ["a", "b", "c"];
    }

    edges(): Array<[string, string]> {
        return [
            ["a", "b"],
            ["b", "c"],
            ["a", "c"],
        ];
    }
}

export class NoEdges3 implements FiniteGraph<number> {
    vertices(): Array<number> {
        return [1, 2, 3];
    }

    edges(): Array<[number, number]> {
        return [];
    }
}

export function is_connected<T>(graph: FiniteGraph<T>): boolean {
    if (graph.vertices().length === 0) {
        return true;
    }

    let node: T = graph.vertices()[0];
    const found = [node];
    for (const path of all_paths_from(graph, node)) {
        for (const n of path) {
            if (!found.includes(n)) {
                found.push(n);
                if (found.length === graph.vertices().length) {
                    return true;
                }
            }
        }
    }
    return found.length === graph.vertices().length;
}

export function reachable_from<T>(graph: FiniteGraph<T>, from: T): Array<T> {
    const ret: Array<T> = [];
    for (const edge of graph.edges()) {
        if (edge[0] === from) {
            ret.push(edge[1]);
        } else if (edge[1] === from) {
            ret.push(edge[0]);
        }
    }
    return ret;
}

function path_includes_edge<T>(path: Array<T>, edge: [T, T]): boolean {
    return to_edges(path).find((e) => edge_equal(e, edge)) !== undefined;
}

export function all_paths_from<T>(
    graph: FiniteGraph<T>,
    node: T
): Array<Array<T>> {
    const ret: Array<Array<T>> = [];
    let i = 0;

    function impl(this_node: T, path_so_far: Array<T>) {
        i++;
        if (i > 1000) {
            throw new Error("TOO MANY PATHS!");
        }
        path_so_far.push(this_node);
        ret.push(path_so_far);
        for (const other_node of reachable_from(graph, this_node)) {
            if (!path_includes_edge(path_so_far, [this_node, other_node])) {
                impl(other_node, path_so_far.slice());
            }
        }
    }

    impl(node, []);

    return ret;
}

function includes_edge<T>(graph: FiniteGraph<T>, edge: [T, T]): boolean {
    return graph.edges().find((e) => edge_equal(edge, e)) !== undefined;
}

function to_edges<T>(path: Array<T>): Array<[T, T]> {
    const ret: Array<[T, T]> = [];
    for (let i = 0; i < path.length - 1; i++) {
        ret.push([path[i], path[i + 1]]);
    }
    return ret;
}

function edge_equal<T>(left: [T, T], right: [T, T]): boolean {
    return (
        (left[0] === right[0] && left[1] === right[1]) ||
        (left[0] === right[1] && left[1] === right[0])
    );
}

function all_edges_equal<T>(
    left: Array<[T, T]>,
    right: Array<[T, T]>
): boolean {
    // Same length
    if (left.length !== right.length) {
        return false;
    }
    // Everything in left is in right
    for (const edge of left) {
        if (right.find((e) => edge_equal(e, edge)) === undefined) {
            return false;
        }
    }
    // and everything in right is in left
    for (const edge of right) {
        if (left.find((e) => edge_equal(e, edge)) === undefined) {
            return false;
        }
    }

    return true;
}

function is_eulerian_circuit<T>(
    graph: FiniteGraph<T>,
    path: Array<T>
): boolean {
    // This is a circuit
    if (path[0] !== path[path.length - 1]) {
        return false;
    }

    // It uses all edges in the graph
    const edges_in_path = to_edges(path);
    return all_edges_equal(edges_in_path, graph.edges());
}

export function has_eulerian_circuit_slow<T>(graph: FiniteGraph<T>): boolean {
    if (graph.vertices().length === 0) {
        return true;
    }

    // The graph is connected
    if (!is_connected(graph)) {
        return false;
    }

    const first_node = graph.vertices()[0];
    return (
        all_paths_from(graph, first_node).find((path) =>
            is_eulerian_circuit(graph, path)
        ) !== undefined
    );
}

function has_even_degree<T>(graph: FiniteGraph<T>, node: T) {
    return reachable_from(graph, node).length % 2 === 0;
}

export function has_eulerian_circuit<T>(graph: FiniteGraph<T>): boolean {
    return (
        is_connected(graph) &&
        graph.vertices().every((v) => has_even_degree(graph, v))
    );
}

export function random_graph(): FiniteGraph<number> {
    const num_vertices = 1 + Math.floor(Math.random() * 4);
    const max_edges = Math.floor((num_vertices * (num_vertices - 1)) / 2);
    const num_edges = Math.floor(Math.random() * (max_edges + 1));
    const vertices: Array<number> = [];
    const edges: Array<[number, number]> = [];
    for (let i = 0; i < num_vertices; i++) {
        vertices.push(i);
    }
    // Populate all edges, then remove at random
    for (let x = 0; x < num_vertices; x++) {
        for (let y = x + 1; y < num_vertices; y++) {
            edges.push([x, y]);
        }
    }
    const edges_to_remove = max_edges - num_edges;
    for (let i = 0; i < edges_to_remove && edges.length > 0; i++) {
        const to_remove = Math.floor(edges.length * Math.random());
        edges.splice(to_remove, 1);
    }

    class RandomGraph implements FiniteGraph<number> {
        vertices(): number[] {
            return vertices;
        }
        edges(): [number, number][] {
            return edges;
        }
    }

    return new RandomGraph();
}
