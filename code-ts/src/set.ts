export interface Set {
    contains(element: any): boolean;
}

export abstract class IterableSet implements Set, Iterable<any> {
    abstract contains(element: any): boolean;

    abstract [Symbol.iterator](): Iterator<any, any, undefined>;

    is_subset_of(other: Set): boolean {
        for (const element of this) {
            if (!other.contains(element)) {
                return false;
            }
        }
        return true;
    }
}

export class EmptySet implements Set {
    contains(_element: any): boolean {
        return false;
    }
}

export class Set123 extends IterableSet {
    contains(element: any): boolean {
        return element === 1 || element === 2 || element === 3;
    }

    [Symbol.iterator](): Iterator<number> {
        return [1, 2, 3].values();
    }
}

export class ArrSet extends IterableSet {
    constructor(private members: any[]) {
        super();
    }

    contains(element: any): boolean {
        return this.members.includes(element);
    }

    [Symbol.iterator](): Iterator<any> {
        return this.members.values();
    }
}

export class Union implements Set {
    constructor(private set1: Set, private set2: Set) {}

    contains(element: any): boolean {
        return this.set1.contains(element) || this.set2.contains(element);
    }
}

export class Intersection implements Set {
    constructor(private set1: Set, private set2: Set) {}

    contains(element: any): boolean {
        return this.set1.contains(element) && this.set2.contains(element);
    }
}

export class AllSets implements Set {
    contains(element: any): boolean {
        // We consider anything with a contains property to be a set!
        return !!element.contains;
    }
}

export class Evens implements Set {
    contains(element: any): boolean {
        return typeof element === "number" && element % 2 === 0;
    }

    *[Symbol.iterator](): Iterator<any> {
        yield 0;
        let i = 1;
        while (true) {
            yield i * 2;
            yield -i * 2;
            i++;
        }
    }
}
