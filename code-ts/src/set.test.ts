import { expect, test } from "vitest";

import {
    AllSets,
    ArrSet,
    EmptySet,
    Evens,
    Intersection,
    Set123,
    Union,
} from "./set.js";

test("The empty set contains nothing", () => {
    const s = new EmptySet();
    expect(s.contains(3)).toBe(false);
    expect(s.contains("f")).toBe(false);
    expect(s.contains(new EmptySet())).toBe(false);
});

test("Set123 only contains 1, 2, 3", () => {
    const s = new Set123();
    expect(s.contains(1)).toBe(true);
    expect(s.contains(3)).toBe(true);
    expect(s.contains(0)).toBe(false);
    expect(s.contains(4)).toBe(false);
    expect(s.contains("f")).toBe(false);
    expect(s.contains(new EmptySet())).toBe(false);
});

test("ArrSet contains array members", () => {
    const s = new ArrSet(["X", "Y", "Z"]);
    expect(s.contains("A")).toBe(false);
    expect(s.contains("X")).toBe(true);
    expect(s.contains("Y")).toBe(true);
    expect(s.contains("Z")).toBe(true);
    expect(s.contains(3)).toBe(false);
    expect(s.contains(new EmptySet())).toBe(false);
});

test("Union contains elements from its sets", () => {
    const s123 = new Set123();
    const s345 = new ArrSet([3, 4, 5]);
    const union = new Union(s123, s345);
    expect(union.contains(0)).toBe(false);
    expect(union.contains(1)).toBe(true);
    expect(union.contains(5)).toBe(true);
    expect(union.contains(6)).toBe(false);
});

test("Intersection only contains elements from both", () => {
    const s123 = new Set123();
    const s345 = new ArrSet([3, 4, 5]);
    const union = new Intersection(s123, s345);
    expect(union.contains(0)).toBe(false);
    expect(union.contains(2)).toBe(false);
    expect(union.contains(3)).toBe(true);
    expect(union.contains(4)).toBe(false);
});

test("Prove union contains all elements", () => {
    const s123 = new Set123();
    const sabc = new ArrSet(["a", "b", "c"]);
    const union = new Union(s123, sabc);

    for (const e of s123) {
        expect(union.contains(e)).toBe(true);
    }

    for (const e of sabc) {
        expect(union.contains(e)).toBe(true);
    }
});

test("Evens contains only even numbers", () => {
    const evens = new Evens();
    expect(evens.contains(1)).toBe(false);
    expect(evens.contains(2)).toBe(true);
    expect(evens.contains(101)).toBe(false);
    expect(evens.contains(102)).toBe(true);
});

test("Iterating some of Evens", () => {
    const evens = new Evens();
    const collected = [];
    for (const c of evens) {
        if (collected.length >= 5) {
            break;
        }
        collected.push(c);
    }
    expect(collected).toStrictEqual([0, 2, -2, 4, -4])
});

test("246 is a subset of evens", () => {
    const evens = new Evens();
    const set123 = new Set123();
    const set246 = new ArrSet([2, 4, 6]);
    expect(set246.is_subset_of(evens)).toBe(true);
    expect(set123.is_subset_of(evens)).toBe(false);
});

test("AllSets contains all sets", () => {
    const s = new AllSets();

    expect(s.contains(new EmptySet())).toBe(true);
    expect(s.contains(new Set123())).toBe(true);
    expect(s.contains(3)).toBe(false);
});

test("AllSets is a member of itself", () => {
    const s = new AllSets();
    expect(s.contains(s)).toBe(true);
});
