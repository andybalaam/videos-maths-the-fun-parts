use crate::set::{IterableSet, Set, VecSet};

trait Group {
    type SetType: Set;
    fn set() -> Set;
    fn op(left: &Self::Item, right: &Self::SetType::Item) -> Self::SetType::Item;
    fn identity() -> Self::Item;
}

trait IterableGroup: Group
where
    Group::SetType: IterableSet,
{
    fn iterable_set() -> <Self as Group>::SetType;
}

struct PlusMod5 {}

impl PlusMod5 {
    fn members() -> VecSet<u8> {
        VecSet::new(vec![0, 1, 2, 3, 4])
    }
}

impl Group for PlusMod5 {
    type SetType = VecSet<u8>;

    fn op(left: &Self::SetType::Item, right: &Self::SetType::Item) -> u8 {
        (*left + *right) % 5
    }

    fn set() -> Self::SetType {
        Self::members()
    }

    fn identity() -> u8 {
        0
    }
}

impl IterableGroup for PlusMod5 {
    fn iterable_set() -> Box<dyn IterableSet<Item = Self::Item>> {
        Box::new(Self::members())
    }
}

struct Unclosed {}

impl Unclosed {
    fn members_set() -> VecSet<u8> {
        VecSet::new(vec![0, 1, 2])
    }
}

impl Group for Unclosed {
    type SetType = VecSet<u8>;

    fn set() -> &Self::SetType {
        Self::members_set()
    }

    fn op(left: &Self::Item, right: &Self::Item) -> Self::Item {
        // No modulo operator - not closed!
        *left + *right
    }

    fn identity() -> Self::Item {
        0
    }
}

impl IterableGroup for Unclosed {
    fn iterable_set() -> Box<dyn IterableSet<Item = Self::Item>> {
        Box::new(Self::members_set())
    }
}

pub fn group_main() {}

fn check_closed<G>() -> Result<(), String>
where
    G: IterableGroup,
    <G as IterableGroup>::Set::Item: std::fmt::Debug,
{
    let s = G::iterable_set();
    for x in s.iter() {
        for y in s.iter() {
            let answer = G::op(&x, &y);
            if !s.contains(&answer) {
                return Err(format!("Not closed: op({x:?}, {y:?}) = {s:?}"));
            }
        }
    }
    true
}

fn identity_works<G>() -> bool
where
    G: IterableGroup,
    <G as Group>::Item: PartialEq<<G as Group>::Item>,
{
    let identity = G::identity();
    for x in G::iterable_set().iter() {
        let xi = G::op(&x, &identity);
        let ix = G::op(&identity, &x);
        if xi != x || ix != x {
            return false;
        }
    }
    true
}

fn element_has_inverse<G>(x: <G as Group>::Item) -> bool
where
    G: IterableGroup,
    <G as Group>::Item: PartialEq<<G as Group>::Item>,
{
    for y in G::iterable_set().iter() {
        if G::op(&x, &y) == G::identity() {
            return true;
        }
    }
    false
}

fn has_all_inverses<G>() -> bool
where
    G: IterableGroup,
    <G as Group>::Item: PartialEq<<G as Group>::Item>,
{
    for x in G::iterable_set().iter() {
        if !element_has_inverse::<G>(x) {
            return false;
        }
    }
    true
}

fn is_associative<G>() -> bool
where
    G: IterableGroup,
{
    let s = G::iterable_set();
    for x in s.iter() {
        for y in s.iter() {
            for z in s.iter() {
                let answer1 = G::op(&x, &G::op(&y, &z));
                let answer2 = G::op(&G::op(&x, &y), &z);
                if answer1 != answer2 {
                    return false;
                }
            }
        }
    }
    true
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn plus_mod_5_is_closed() {
        check_closed::<PlusMod5>()?;
    }

    #[test]
    fn plus_mod_5_identity_works() {
        assert!(identity_works::<PlusMod5>());
    }

    #[test]
    fn plus_mod_5_has_inverses() {
        assert!(has_all_inverses::<PlusMod5>());
    }

    #[test]
    fn plus_mod_5_is_associative() {
        assert!(is_associative::<PlusMod5>());
    }

    #[test]
    fn plus_without_mod_is_not_closed() {
        assert!(!is_closed::<Unclosed>());
    }

    #[test]
    fn plus_mod_5_is_commutative() {
        for x in PlusMod5::members() {
            for y in PlusMod5::members() {
                let xy = PlusMod5::op(&x, &y);
                let yx = PlusMod5::op(&y, &x);
                assert_eq!(xy, yx);
            }
        }
    }
}
