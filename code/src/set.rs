use std::marker::PhantomData;

pub trait Set {
    type Item;
    fn contains(&self, element: &Self::Item) -> bool;
}

pub trait IterableSet: Set {
    fn iter(&self) -> Box<dyn Iterator<Item = Self::Item> + '_>;
}

trait Subsetable: IterableSet {
    fn is_subset_of(&self, other: impl Set<Item = Self::Item>) -> bool {
        self.iter().all(|element| other.contains(&element))
    }
}

struct Set123 {}

impl Set for Set123 {
    type Item = i32;
    fn contains(&self, element: &i32) -> bool {
        *element == 1 || *element == 2 || *element == 3
    }
}

impl IterableSet for Set123 {
    fn iter(&self) -> Box<dyn Iterator<Item = i32> + '_> {
        Box::new(vec![1, 2, 3].into_iter())
    }
}

impl Subsetable for Set123 {}

struct EmptySet<T> {
    _t: PhantomData<T>,
}

impl<T> EmptySet<T> {
    fn new() -> Self {
        Self {
            _t: Default::default(),
        }
    }
}

impl<T> Set for EmptySet<T> {
    type Item = T;
    fn contains(&self, _element: &T) -> bool {
        false
    }
}

pub struct VecSet<T>
where
    T: PartialEq<T>,
{
    members: Vec<T>,
}

impl<T> VecSet<T>
where
    T: PartialEq<T>,
{
    pub fn new(members: Vec<T>) -> Self {
        Self { members }
    }
}

impl<T> Set for VecSet<T>
where
    T: PartialEq<T>,
{
    type Item = T;

    fn contains(&self, element: &T) -> bool {
        self.members.contains(element)
    }
}

struct AllSets<T> {
    _t: PhantomData<T>,
}

impl<T> Set for AllSets<T>
where
    T: Set,
{
    type Item = T;

    fn contains(&self, _element: &T) -> bool {
        true
    }
}

impl<T> IterableSet for VecSet<T>
where
    T: PartialEq<T> + Clone,
{
    fn iter(&self) -> Box<dyn Iterator<Item = Self::Item> + '_> {
        Box::new(self.members.iter().cloned())
    }
}

impl<T> Subsetable for VecSet<T> where T: PartialEq<T> + Clone {}

struct Union<S>
where
    S: Set,
{
    first: S,
    second: S,
}

impl<S> Union<S>
where
    S: Set,
{
    fn new(first: S, second: S) -> Self {
        Self { first, second }
    }
}

impl<S> Set for Union<S>
where
    S: Set,
{
    type Item = <S as Set>::Item;

    fn contains(&self, element: &Self::Item) -> bool {
        self.first.contains(element) || self.second.contains(element)
    }
}

struct Intersection<T>
where
    T: Set,
{
    first: T,
    second: T,
}

impl<T> Intersection<T>
where
    T: Set,
{
    fn new(first: T, second: T) -> Self {
        Self { first, second }
    }
}

impl<T> Set for Intersection<T>
where
    T: Set,
{
    type Item = <T as Set>::Item;

    fn contains(&self, element: &Self::Item) -> bool {
        self.first.contains(element) && self.second.contains(element)
    }
}

struct Evens {}

impl Set for Evens {
    type Item = i32;
    fn contains(&self, element: &Self::Item) -> bool {
        *element % 2 == 0
    }
}

pub fn set_main() {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn set123_contains_1_but_not_4() {
        let set = Set123 {};
        assert!(set.contains(&1));
        assert!(!set.contains(&4));
    }

    #[test]
    fn emptyset_contains_nothing() {
        let empty = EmptySet::new();
        assert!(!empty.contains(&"d"));

        let empty = EmptySet::new();
        assert!(!empty.contains(&5));
    }

    #[test]
    fn vecset_contains_only_its_elements() {
        let set = VecSet {
            members: vec![3, 4, 5],
        };
        assert!(!set.contains(&1));
        assert!(set.contains(&4));
    }

    #[test]
    fn union_contains_elements_from_its_arguments() {
        // I don't think I can allow the two Set types to be different, even if
        // I go completely dynamic and hold everything in Boxes.
        let set123 = VecSet {
            members: vec![1, 2, 3],
        };
        let set345 = VecSet {
            members: vec![3, 4, 5],
        };
        let union = Union::new(set123, set345);
        assert!(!union.contains(&0));
        assert!(union.contains(&1));
        assert!(union.contains(&5));
        assert!(!union.contains(&6));
    }

    #[test]
    fn prove_union_contains_elements_from_its_arguments() {
        let set123a = VecSet {
            members: vec![1, 2, 3],
        };
        let set345 = VecSet {
            members: vec![3, 4, 5],
        };
        let union = Union::new(set123a, set345);

        let set123b = VecSet {
            members: vec![1, 2, 3],
        };
        for element in set123b.iter() {
            assert!(union.contains(&element));
        }
    }

    #[test]
    fn intersection_only_contains_elements_in_both() {
        let set123 = VecSet {
            members: vec![1, 2, 3],
        };
        let set345 = VecSet {
            members: vec![3, 4, 5],
        };
        let intersection = Intersection::new(set123, set345);
        assert!(!intersection.contains(&0));
        assert!(!intersection.contains(&1));
        assert!(intersection.contains(&3));
        assert!(!intersection.contains(&5));
        assert!(!intersection.contains(&6));
    }

    #[test]
    fn evens_contains_only_even_numbers() {
        let evens = Evens {};
        assert!(evens.contains(&4));
        assert!(evens.contains(&2));
        assert!(evens.contains(&0));
        assert!(evens.contains(&-2));
        assert!(evens.contains(&-768));

        assert!(!evens.contains(&1));
        assert!(!evens.contains(&-1));
        assert!(!evens.contains(&-767));
    }

    #[test]
    fn is_subset_of_can_say_246_fits_inside_evens() {
        let set246 = VecSet {
            members: vec![2, 4, 6],
        };
        let evens = Evens {};
        assert!(set246.is_subset_of(evens));
    }

    #[test]
    fn is_subset_of_can_discover_that_123_is_not_even() {
        let set123 = Set123 {};
        let evens = Evens {};
        assert!(!set123.is_subset_of(evens));
    }

    #[test]
    fn any_set_is_a_member_of_allsets() {
        let set123 = Set123 {};
        let all_sets = AllSets {
            _t: Default::default(),
        };
        assert!(all_sets.contains(&set123));
    }

    // Can't do this - circular type definition
    //#[test]
    //fn allsets_is_a_member_of_itself() {
    //    let all_sets = AllSets {
    //        _phantom_data_s: Default::default(),
    //        _phantom_data_t: Default::default(),
    //    };
    //    assert!(all_sets.contains(&all_sets));
    //}
}
