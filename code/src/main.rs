mod group;
mod set;

use group::group_main;
use set::set_main;

fn main() {
    set_main();
    group_main();
}
